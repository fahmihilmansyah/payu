<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="<?php echo \yii\helpers\Url::to(['/bootstrap/css/bootstrap.min.css']) ?>"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="<?php echo \yii\helpers\Url::to(['/bootstrap/js/bootstrap.min.js']) ?>"
            integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
            crossorigin="anonymous"></script>
    <?php $this->head() ?>
</head>
<body class="bg-secondary">
<?php $this->beginBody() ?>
<div class="container" style="padding-top: 60px;">
    <div class="card">
        <div class="d-flex flex-column card-body" style="min-height: 85vh!important;">
            <div class="row">
                <div class="col-md-4 d-none d-sm-block">
                    <div class="list-group">
                        <span href="#" class="list-group-item">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg"
                                         width="100" class="rounded">
                                </div>
                                <div class="col-md-8">
                                    <span><b>Fahmi Hilmansyah</b><hr/></span>
                                    <span style="font-size: 20px"><b>Rp. 50.000</b></span>
                                    <label class="btn btn-sm btn-success float-right">Top Up</label>
                                </div>
                            </div>
                        </span>
                        <a href="#" class="list-group-item list-group-item-action active">
                            Profile
                        </a>
                        <a href="#" class="list-group-item list-group-item-action">Transaksi</a>
                        <a href="#" class="list-group-item list-group-item-action">Logout</a>
                    </div>
                </div>
                <div class="col-md-8 ">
                    <section style="min-height: 80%;">
                        <?= $content ?>
                    </section>
                </div>
            </div>
            <div class="mt-auto text-center">
                <span style="color: #a9a7a7">Member of Dompet Dhuafa</span><br/>
                <span style="color: #a9a7a7; font-size: 12px;">No. Reg: 029/SK/MPZ-DD/III/2019</span>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
