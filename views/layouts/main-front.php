<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="<?php echo \yii\helpers\Url::to(['/bootstrap/css/bootstrap.min.css']) ?>" >
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="<?php echo \yii\helpers\Url::to(['/bootstrap/js/bootstrap.bundle.js']) ?>" ></script>
    <?php $this->head() ?>
</head>
<body class="bg-secondary">
<?php $this->beginBody() ?>
<div class="container" style="padding-top: 60px;">
    <div class="card">
        <div class="d-flex flex-column card-body" style="min-height: 85vh!important;">
            <div class="row">
                <div class="col-md-6 mx-auto" style="margin-top: 10%;">
                    <div class="container">
                        <?= $content ?>
                    </div>
                </div>
            </div>
            <div class="mt-auto text-center">
                <span style="color: #a9a7a7">Member of Dompet Dhuafa</span><br/>
                <span style="color: #a9a7a7; font-size: 12px;">No. Reg: 029/SK/MPZ-DD/III/2019</span>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
