<?php
/**
 * Created by PhpStorm.
 * Project : mupays
 * User: fahmihilmansyah
 * Date: 28/05/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 00.14
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<form id="my_form" action="<?php echo $url_callback; ?>" method="post">
    <?php foreach ($list as $k => $v):
        if ($k != 'url_callback'):?>
            <input type="hidden" name="<?php echo $k ?>" value="<?php echo $v; ?>">
        <?php endif; endforeach; ?>
    <input type="submit" name="submission_button" value="Click here if the site is taking too long to redirect!">
</form>

<script type="text/javascript">
    function submitForm() {
        document.getElementById('my_form').submit();
    }

    window.onload = submitForm;
</script>
