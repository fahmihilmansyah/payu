<?php
/**
 * Created by PhpStorm.
 * Project : mupays
 * User: fahmihilmansyah
 * Date: 22/05/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 23.55
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$token = Yii::$app->request->get('token');
?>

<div class="card" id="loginform">
    <div class="card-body d-flex flex-column">
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i>Saved!</h4>
                <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i>Saved!</h4>
                <?= Yii::$app->session->getFlash('error') ?>
            </div>
        <?php endif; ?>
        <?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/auth/default/login'],true),'POST')?>
        <input type="hidden" name="token_partner" value="<?php echo $token;?>">
            <div class="form-group">
                <label for="InputUsername">Username</label>
                <input type="text" name="username" class="form-control" id="InputUsername" aria-describedby="emailHelp"
                       placeholder="Enter Username">
            </div>
            <div class="form-group">
                <label for="InputPassword">Password</label>
                <input type="password" class="form-control" name="password" id="InputPassword" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-primary form-control">Submit</button>
        <?php echo \yii\helpers\Html::endForm();?>
        <div class="text-right mt-auto">
            <span>Belum punaya akun</span>
            <a href="#" id="btnregister" >Register</a>
        </div>
    </div>
</div>
<div class="card" id="registerform" style="display: none;">
    <div class="card-body d-flex flex-column">
        <?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/auth/default/register'],true),'POST')?>
        <input type="hidden" name="token_partner" value="<?php echo $token;?>">
            <div class="form-group">
                <label for="InputFullname">Full Name</label>
                <input type="text" required name="fullname" class="form-control" id="InputFullname" aria-describedby="fullname"
                       placeholder="Enter Username">
            </div>
            <div class="form-group">
                <label for="InputUsername">Username/Email</label>
                <input type="email" required name="username" class="form-control" id="InputUsername" aria-describedby="emailHelp"
                       placeholder="Enter Username">
            </div>
            <div class="form-group">
                <label for="inputnohp">No Hp</label>
                <input type="text" required name="no_hp" class="form-control" id="inputnohp" aria-describedby="inputnohp"
                       placeholder="Enter No. Handphone">
            </div>
            <div class="form-group">
                <label for="InputPassword">Password</label>
                <input type="password" required class="form-control" name="password" id="InputPassword" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="InputrePassword">Re-Password</label>
                <input type="password" required class="form-control" name="repassword" id="InputrePassword" placeholder="Re-Password">
            </div>
            <button type="submit" class="btn btn-primary form-control">Submit</button>
        <?php echo \yii\helpers\Html::endForm();?>
        <div class="text-right mt-auto">
            <span>Sudah punaya akun? </span>
            <a href="#" id="btnlogins" >Login</a>
        </div>
    </div>
</div>
<script>
    $("#btnregister").on('click',function () {
        $("#loginform").hide();
        $("#registerform").show();
    });
    $("#btnlogins").on('click',function () {
        $("#loginform").show();
        $("#registerform").hide();
    });
    // $(function () {
    // $('#myTab li:last-child a').tab('show')
    // })
</script>