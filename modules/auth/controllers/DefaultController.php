<?php

namespace app\modules\auth\controllers;

use Yii;
use app\fhhlibs\Mylibs;
use GuzzleHttp\Psr7\Request;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Default controller for the `auth` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    function actionLogin($key=''){
        $this->layout = '@app/views/layouts/main-front.php';
        if($_POST){
            $req = Yii::$app->request->post();
            $username = $req['username'];
            $password = $req['password'];
            $token_partner = $req['token_partner'];
            $datasend = array(
                'token_partner'=>$token_partner,
                'username'=>$username,
                'password'=>$password,
            );
            $res = json_decode(Mylibs::login($datasend),true);

            if($res['rc'] != '0000'){
                Yii::$app->session->setFlash('error', $res['msg']);
                return $this->redirect(Url::to(['/auth/default/login','token'=>$token_partner],true));
            }else{
                Yii::$app->session->setFlash('success', $res['msg']);
                if(!empty($res['data']['url_callback'])){
                    $dataredir = [
                        'url_callback'=>$res['data']['url_callback'],
                        'list'=>$res['data']
                    ];
                    return $this->render('redirform',$dataredir);
                    return $this->redirect($res['data']['url_callback']."?ltuser=".$res['data']['login_user']);
                }

                print_r($res);exit;
            }
        }
        return $this->render('form-log-reg');
        echo $key;exit;
    }
    function actionRegister(){
        $req = \Yii::$app->request->post();
        $token_partner= $req['token_partner'];
        $datasend = array(
            'token_partner'=>$req['token_partner'],
            'fullname'=>$req['fullname'],
            'email'=>$req['username'],
            'password'=>$req['password'],
            'no_hp'=>$req['no_hp'],
        );
        $res = json_decode(Mylibs::register($datasend),true);
        if($res['rc'] != '0000'){
            Yii::$app->session->setFlash('error', $res['msg']);
            return $this->redirect(Url::to(['/auth/default/login','token'=>$token_partner],true));
        }else{
            Yii::$app->session->setFlash('success', $res['msg']);

            if(!empty($res['data']['url_callback'])){
                $dataredir = [
                    'url_callback'=>$res['data']['url_callback'],
                    'list'=>$res['data']
                ];
                return $this->render('redirform',$dataredir);
            }

            if(!empty($res['data']['url_callback'])){
                return $this->redirect($res['data']['url_callback']);
            }
        }
        $session = Yii::$app->session;
        $session->set('user_login', $res['data']);
        print_r($_SESSION);
        exit;
    }
}
