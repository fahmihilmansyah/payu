<?php
/**
 * Created by PhpStorm.
 * Project : mupays
 * User: fahmihilmansyah
 * Date: 26/05/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 23.24
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
namespace app\fhhlibs;
 class Mylibs{
     const URLS = 'http://core.mumuapps.id/';
     const HEADER = ['token-key: DDTEKNOPAYMENT',
         'app-key: APPKEYDDTEKNO',
         'Content-Type: application/json',
     ];
     static function register($data=[]){
         $url = self::URLS."mupays/partner/register";
         $mylib = new Mylibs();
        return $mylib->_exec($url, json_encode($data),'POST',self::HEADER);
     }
     static function login($data=[]){
         $url = self::URLS."mupays/partner/login";

         $mylib = new Mylibs();
        return $mylib->_exec($url, json_encode($data),'POST',self::HEADER);
     }
     private function _exec($url = '', $postdata = '', $METHOD = 'POST', $HEADER = []){
         // create curl resource
         $ch = curl_init();
//         exit;

         // set url
         curl_setopt($ch, CURLOPT_URL, $url);
         //return the transfer as a string
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
         curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

         // $output contains the output string
         $output = curl_exec($ch);
//         echo $output;exit;
//         $output = json_decode($output,true);
         // close curl resource to free up system resources
         curl_close($ch);
         return $output;
     }
 }